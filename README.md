<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/code-scrawl/ -->
# LilDb: A little database wrapper for PDO  
Remove a lot of boilerplate code with wrapper functions for common sql verbs.  
  
## Install  
`composer require taeluf/lildb v0.1.x-dev`  
Breaking changes will bring a version bump to `v0.2`. I will create tags for proper release hopefully by the end of April 2021, after I've used it a little bit & I know its stable.  
  
## Usage  
Instantiate with `new` or one of the static methods, then call whatever helper methods you need.  
  
### `class Tlf\LilDb` methods  
  
  
## Development  
This depends upon [Code Scrawl](https://tluf.me/code-scrawl) for documentation and [My Test Lib](https://tluf.me/php-tests), but neither are up on Packagist yet, so they're not listed in the composer.json.  
  
